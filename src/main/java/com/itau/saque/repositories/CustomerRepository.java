package com.itau.saque.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.saque.models.Customer;


public interface CustomerRepository extends CrudRepository<Customer, Long>{
	
	public Customer findByUsername(String name);

}
