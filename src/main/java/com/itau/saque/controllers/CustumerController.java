package com.itau.saque.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.saque.models.Customer;
import com.itau.saque.models.RatesConverter;
import com.itau.saque.models.WithdrawRequest;
import com.itau.saque.repositories.CustomerRepository;

@RestController
public class CustumerController {
	
	@Autowired
	CustomerRepository customerRepository; 
	
	@Autowired
	RatesConverter rates; 

	
	@RequestMapping(method=RequestMethod.POST,path = "/customer")
	public Customer withdraw(@RequestBody WithdrawRequest withdrawRequest) {
		
		Optional<Customer> optionalCustomer = customerRepository.findById(withdrawRequest.getClientId());
				
		double valorConvertido = rates.convert("EUR",withdrawRequest.getCurrency(),withdrawRequest.getAmount());
		
		Customer customer = optionalCustomer.get();
		if(customer.getBalance() >= valorConvertido) {
			customer.setBalance(customer.getBalance() - valorConvertido);
			updateCustomer(customer.getId(), customer);
		}
		
		return customer;
	}
	
	@RequestMapping(method=RequestMethod.POST, path = "/customer/inserir")
	public Customer insertCustomer(@RequestBody Customer customer) {
		return customerRepository.save(customer);		
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path = "customer/deletar/{id}")
	public ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
		
		Optional<Customer> optinalCustomer = customerRepository.findById(id);	
		
		if(!optinalCustomer.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		customerRepository.deleteById(id);	
		
		return ResponseEntity.ok().build();		
	}
	
	@RequestMapping(method=RequestMethod.PUT, path ="/customer/{id}")
	public ResponseEntity<?> updateCustomer(@PathVariable long id, @RequestBody Customer customer){
		
		Optional<Customer> optionalCustomer = customerRepository.findById(id);
		
		if(!optionalCustomer.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Customer customerDoBanco = optionalCustomer.get();
		customerDoBanco.setBalance(customer.getBalance());

		Customer produtoSalvo = customerRepository.save(customerDoBanco);
		
		return ResponseEntity.ok().body(produtoSalvo);
	}
	
	

}
