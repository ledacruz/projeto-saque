package com.itau.saque.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.saque.models.RatesResponse;

@Service
public class FixerIoService {
	private RestTemplate restTemplate = new RestTemplate();

	public RatesResponse getFixerIo(String pais) {

		String url = "http://data.fixer.io/api/latest?access_key=e539e11f4de63138df2cea4a75a4ad58" + "&base=EUR&symbols=" + pais;
		return restTemplate.getForObject(url, RatesResponse.class);
	}
}
