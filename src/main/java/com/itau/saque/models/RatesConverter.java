package com.itau.saque.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.saque.service.FixerIoService;

@Service
public class RatesConverter {

	private RatesResponse ratesResponse;
	
	@Autowired
	FixerIoService fixerIoService;
	
	public RatesResponse getRatesResponse() {
		return ratesResponse;
	}

	public void setRatesResponse(RatesResponse ratesResponse) {
		this.ratesResponse = ratesResponse;
	}

	public double convert(String from, String to, double amount) {
		setRatesResponse(fixerIoService.getFixerIo(to));
		System.out.println(ratesResponse.getRates());
		
		double valorCotacao = 0.0;
		for(Double rates: ratesResponse.getRates().values()) {
			valorCotacao = rates;
			break;
		}
		
		return amount * valorCotacao;

	}

}
