package com.itau.saque.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	@Size(max=50)
	private String name;
	
	@NotNull
	private String username;

	@NotNull
	private String password;
	
	@NotNull
	private double balance;
	
//	@ManyToOne
//	private WithdrawRequest withdrawRequest; 

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

//	public WithdrawRequest getWithdrawRequest() {
//		return withdrawRequest;
//	}
//
//	public void setWithdrawRequest(WithdrawRequest withdrawRequest) {
//		this.withdrawRequest = withdrawRequest;
//	}
	
	
	
	
	
	
	
}
